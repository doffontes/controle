// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyABBhfEJqEwNTreCDP6maI32iPwG_G2FiM',
    authDomain: 'controle-david.firebaseapp.com',
    databaseURL: 'https://controle-david.firebaseio.com',
    projectId: 'controle-david',
    storageBucket: 'controle-david.appspot.com',
    messagingSenderId: '558239822400',
    appId: '1:558239822400:web:13efad92c2b56d7ff2e5a5',
    measurementId: 'G-Q05GKQZCK5'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
