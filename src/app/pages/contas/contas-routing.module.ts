import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {CadastroPage} from "./cadastro/cadastro.page";
import {RelatorioPage} from "./relatorio/relatorio.page";
import {CommonModule} from "@angular/common";
import {IonicModule} from "@ionic/angular";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {ListaPage} from "./lista/lista.page";

const routes: Routes = [
  { path: '', children: [
      {path: 'pagar', component: ListaPage},
      {path: 'receber', component: ListaPage},
      {path: 'cadastro', component: CadastroPage},
      {path: 'relatorio', component: RelatorioPage},
    ]},
  {
    path: 'lista',
    loadChildren: () => import('./lista/lista.module').then( m => m.ListaPageModule)
  }
];

@NgModule({
  imports: [
      RouterModule.forChild(routes),
      CommonModule,
      IonicModule,
      FormsModule,
      ReactiveFormsModule
  ],
  declarations: [
      CadastroPage,
      RelatorioPage
  ]
})
export class ContasRoutingModule { }
